# nativeshare

[![CI Status](https://img.shields.io/travis/aalinovsky/nativeshare.svg?style=flat)](https://travis-ci.org/aalinovsky/nativeshare)
[![Version](https://img.shields.io/cocoapods/v/nativeshare.svg?style=flat)](https://cocoapods.org/pods/nativeshare)
[![License](https://img.shields.io/cocoapods/l/nativeshare.svg?style=flat)](https://cocoapods.org/pods/nativeshare)
[![Platform](https://img.shields.io/cocoapods/p/nativeshare.svg?style=flat)](https://cocoapods.org/pods/nativeshare)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

nativeshare is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'nativeshare'
```

## Author

aalinovsky, dvd2444@mail.ru

## License

nativeshare is available under the MIT license. See the LICENSE file for more info.
